pragma solidity ^0.4.18;

    contract Coin is Owned {
    
    string public name;
    string public symbol;
    uint8 public decimals;

    /* This creates an array with all balances */
    mapping (address => uint256) public balanceOf;

	event Transfer(address indexed from, address indexed to, uint256 value);

    /* Constructor */
    function Coin(uint256 initialSupply, string tokenName, string tokenSymbol, uint8 decimalUnits) public{
        balanceOf[msg.sender] = initialSupply;
        name = tokenName;                                   // Set the name for display purposes
        symbol = tokenSymbol;                               // Set the symbol for display purposes
        decimals = decimalUnits;                            // Amount of decimals for display purpose
    }

        /* Send coins */
    function transfer(address _from, address _to, uint256 _value) internal {
        require (balanceOf[_to] + _value > balanceOf[_to]); // Check for overflow

        /* Check if sender has balance and for overflows */
        require(balanceOf[msg.sender] >= _value && balanceOf[_to] + _value >= balanceOf[_to]);

        /* Add and subtract new balances */
        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;

         /* Notify anyone listening that this transfer took place */
        Transfer(msg.sender, _to, _value);
    }

}
    contract Owned {
        address public owner;

        function Owned() public{
            owner = msg.sender;
        }

        modifier onlyOwner {
            require(msg.sender == owner);
            _;
        }

        function transferOwnership(address newOwner) public onlyOwner {
            owner = newOwner;
        }
    }

